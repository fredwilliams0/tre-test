/*
	Created: RAP - December 2016 for first release
	Purpose: provide context for Claimant detail page
*/

public with sharing class ClaimantController {
//	public static final strings

//	other variables
	public boolean isValid;
	public string selectedLien{get;set;}
	public boolean editable{get;set;}
	public list<Claimant__c> claimants{get;set;}
	
	public string ssn {
		get {
			if (ssn == null)
				ssn = ApexPages.currentPage().getParameters().get('ssn');
			return ssn;
		}
		set;
	}
	public Claimant__c claimant {
		get {
			if (claimant == null) {
				if (!string.isBlank(ssn)) {
					claimant = [SELECT Id, Name, Address__c, City__c, Email__c, Law_Firm__c, Phone__c, SSN__c, State__c, Zip__c,
									   DOB__c, DOD__c, Gender__c, ProvidioID__c, Status__c, Law_Firm__r.Name, Law_Firm__r.Phone, 
									   Law_Firm__r.Website
								FROM Claimant__c
								WHERE SSN__c = :ssn];
					claimants = new list<Claimant__c>{claimant};	
				}
			}
			return claimant;
		}
		set;
	}
	public string actionId {
		get {
			if (actionId == null)
				actionId = ApexPages.currentPage().getParameters().get('aid');
			return actionId;
		}
		set;
	}
	public Action__c action {
		get {
			if (action == null) {
				if (!string.isBlank(actionId)) {
					action = [SELECT Active__c, All_Private_Liens__c, Asserted_Private_Liens_Only__c, Case_Name__c, Fee_Structure_Plus__c, Id, 
									 Law_Firm_Attorney_Contact__c, Law_Firm_Paralegal_Contact__c, Law_Firm__c, Medicaid_Eligibility_Verification__c, 
									 Medicare_Eligibility_Verification__c, Medicare_Model__c, Medicare_Part_C__c, Medicare_Single_Event__c, 
									 Medicare_Special_Project__c, MSP_Compliance__c, MT_as_SE__c, Name, Number_of_Claimants__c, PLRP__c, Providio_Generated__c,
									 QSF_Administration__c, Salesperson1__c, Salesperson2__c, Salesperson3__c, SRP_Generated__c, Unknown_follow_up_needed__c 
							  FROM Action__c
							  WHERE Id = :actionId];
				}
			}
			return action;
		}
		set;
	}
	public list<Injury__c> injuries {
		get {
			if (injuries == null && actionId != null) {
				injuries = [SELECT Action__c, Compensable__c, Claimant__c, Description_Long__c, Explant_Date__c, ICD_Code__c, Id, Implant_Date__c, Injury_Category__c,
								   Injury_Description__c, Last_Treatment_Date__c, Lien__c, Name, Non_Surgical_Treatment_Enhancer__c, Non_Surgical_Treatment__c,
								   Surgical_Facility__c, Surgical_Treatment_Enhancer__c, Total_Surgery_Per_Defendant__c, Treatment_Enhancer__c, Base_Category__c,
								   Settlement_Category__c
							FROM Injury__c
							WHERE Claimant__c = :claimant.Id
							AND Action__c = :actionId];	
			}
			return injuries;
		}
		set;
	}
	public Injury__c injury {
		get {
			if (injuries != null && !injuries.isEmpty())
				injury = injuries[0];
			return injury;
		}
		set;
	}
	public string injuryDesc {
		get {
			if (injuryDesc == null) 
				injuryDesc = injury.Description_Long__c;
			return injuryDesc;
		}
		set;
	}
	public GeneralLedger__c gl {
		get {
			if (gl == null) {
				try {
					gl = [SELECT Id, Name, Account__c, Action__c, Balance__c, Claimant__c, Contact__c 
						  FROM GeneralLedger__c
						  WHERE Claimant__c = :claimant.Id
						  AND Action__c = :actionId];
				}
				catch (exception e) {}
			}
			return gl;
		}
		set;
	}
	public list<Lien__c> liens {
		get {
			if (liens == null && gl != null) {
				liens = [SELECT Account__c, Action__c, Claimant__c, Cleared__c, Date_No_Interest__c, Date_Payment__c,
							    Date_Re_sweep_Sent__c, Date_Submitted__c, Deal_Made__c, Eligible__c, Final_Demand_Amount__c,
							    Final_Demand_Received__c, Final_Lien_Amt_Paid__c, General_Ledger__c, Health_Plan__c, HICN__c,
							    Id, LienAmount__c, Lien_Holder_File_ID__c, Lien_Type__c, Name, Notes__c, Payable_Entity__c,
							    Policy__c, Re_sweep_Sent__c, Stages__c, State__c, Status_ERISA__c, Status__c, Submitted__c,
							    Subro_Rep__c
						 FROM Lien__c
						 WHERE General_Ledger__c = :gl.Id];	
			}
			return liens;
		}
		set;
	}
	
// constructor
	public ClaimantController() {
		if (string.isBlank(ApexPages.currentPage().getParameters().get('aid')) ||
			string.isBlank(ApexPages.currentPage().getParameters().get('ssn'))) {
			ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, 'Missing URL parameters. Please contact your system administrator.'));
		}
	}
	
// action methods
	
	public pageReference Save() {
		Validate();
		if (!isValid)
			return null;
		database.saveResult sr;
		try {
			sr = database.update(claimant);
			editable = false;
		}
		catch (exception e) {
			ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, 'Exception caught saving Claimant: ' + e.getMessage()));
		}
		if (sr != null && !sr.isSuccess()) {
			ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, 'Claimant save failed: ' + sr.getErrors()));
			editable = true;
		}
		return null;
	}
	
	public void Validate() {
		isValid = true;
	}
	
	public pageReference Edit() {
		editable = true;
		return null;
	}
	
	public pageReference RedirectToLienDetail() {
		pageReference pg = new pageReference('apex/LienDetail');
		map<string,string> params = pg.getParameters();
		params.put('id', selectedLien);
		return pg.setRedirect(true);
	}
}