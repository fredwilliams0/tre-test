/*
	Created: RAP - December 2016 for first release
	Purpose: test function of LienController_Aura
	Coverage as of 12/30/16 - 97% 
*/
@isTest
public with sharing class LienController_Aura_Test {

@testSetup
	public static void CreateData() {
    	Account a = new Account(Name = 'Test Account',
    							BillingStreet = '123 Main St',
    							BillingCity = 'Denver',
    							BillingState = 'CO',
    							BillingPostalCode = '80202');
    	Account b = new Account(Name = 'Payable Entity',
    							BillingStreet = '345 Elm St',
    							BillingCity = 'Portland',
    							BillingState = 'OR',
    							BillingPostalCode = '50303');
    	insert new list<Account>{a,b};
    	Contact c = new Contact(LastName = 'Crain',
    							email = 'denny@crain.com');
    	insert c;
    	Action__c action = new Action__c(Name = 'Test Action',
    									 Active__c = true,
    									 Law_Firm__c = a.Id,
    									 Law_Firm_Attorney_Contact__c = c.Id);
		insert action;
		Claimant__c claim = new Claimant__c(Address__c = '123 Main St',
											City__c = 'Denver',
											Email__c = 'rap@rap.com',
											Law_Firm__c = a.Id,
											Name = 'RAP',
											Phone__c = '3035551212',
											SSN__c = '135461448',
											State__c = 'CO',
											Zip__c = '80138');
		insert claim;
		Id aId = action.Id;
		Id cId = claim.Id;
		GeneralLedger__c gl = new GeneralLedger__c(Account__c = a.Id, 
												   Action__c = aId, 
												   Claimant__c = cId);
		insert gl;
		Lien__c lien1 = new Lien__c(Account__c = a.Id, 
									Action__c = aId, 
									Claimant__c = cId, 
									Cleared__c = false,
									Date_Submitted__c = system.today(), 
									General_Ledger__c = gl.Id, 
									Lien_Type__c = 'Private Non PLRP', 
									Notes__c = 'This is a note', 
									Payable_Entity__c = b.Id,
									Stages__c = 'Submitted', 
									State__c = 'CO', 
									Status_ERISA__c = 'MCO', 
									Submitted__c = true);
		insert lien1;
		map<integer,string> lienMap = new map<integer,string>();
		list<Lien_Negotiated_Amounts__c> lienList = new list<Lien_Negotiated_Amounts__c>();
		Date today = system.today();
		lienMap.put(0, 'Asserted');
		lienMap.put(1, 'Audit');
		lienMap.put(2, 'Neg - Proposed');
		lienMap.put(3, 'Neg - Response');
		lienMap.put(4, 'Contested');
		lienMap.put(5, 'Final');
		lienMap.put(6, 'Paid');
		for (integer i=0;i<7;i++) {
			string str = lienMap.get(i);
			Date dateStr = today.addMonths(i);
			Lien_Negotiated_Amounts__c lnac = new Lien_Negotiated_Amounts__c(Lien__c = lien1.Id, 
																			 Lien_Amount__c = 50000-(i*2500), 
																			 Lien_Amount_Date__c = dateStr, 
																			 Phase__c = lienMap.get(i));
			lienList.add(lnac);
		}
		insert lienList;
	}
    static testMethod void TestController() {
    	Lien__c l = new Lien__c();
		test.startTest();
		ApexPages.StandardController stdctrl = new ApexPages.StandardController(l);
    	LienController_Aura ctrl = new LienController_Aura(stdctrl);
    	system.assert(LienController_Aura.fault);
    	string var1;
    	try {
	    	var1 = LienController_Aura.Save();
    	}
    	catch (exception e) {}
    	l = [SELECT Id FROM Lien__c WHERE Lien_Type__c = 'Private Non PLRP' limit 1];
    	ApexPages.currentPage().getParameters().put('id', l.Id);
    	ctrl = new LienController_Aura(stdctrl);
    	var1 = LienController_Aura.Save();
    	system.assertEquals('Success', var1);
		Lien__c lCheck = LienController_Aura.getLiens(l.Id);
		list<Injury__c> iList = LienController_Aura.getInjuries(l.Id);
		list<Award__c> aList = LienController_Aura.getAwards(l.Id);
    	LienController_Aura.selectedLienAmt = LienController_Aura.lienAmtList[0].Id;
    	string pg1 = LienController_Aura.EditLHA();
    	system.assert(pg1.contains(LienController_Aura.selectedLienAmt));
    	LienController_Aura.DeleteLHA();
    	list<Lien_Negotiated_Amounts__c> checkList = LienController_Aura.getlienAmtList(l.Id);
    	system.assertEquals(6,checkList.size());
    	Lien_Negotiated_Amounts__c var2 = LienController_Aura.lienAmt;
    	var1 = LienController_Aura.Save();
    	list<Lien_Negotiated_Amounts__c> l1 = LienController_Aura.getlienAmtList(l.Id); 
    	var1 = LienController_Aura.cancelReturn;
    	LienController_Aura.editable = true;
    	var1 = LienController_Aura.cancelReturn;
    	LienController_Aura.Cancel();
    	test.stopTest();
    }
}