/*
	Created: RAP - January 2017 for first release
	Purpose: provide context for Lightning Components 
*/
global with sharing class Controller_Aura1 {

// constructor
	public Controller_Aura1(ApexPages.StandardController sc) {}
	
// action methods
	
@auraEnabled
	global static Lien__c getLien(string lId) {
		return [SELECT Account__c, Action__c, Claimant__c, Cleared__c, Date_No_Interest__c, Date_Payment__c, Substage__c, 
					   Date_Re_sweep_Sent__c, Date_Submitted__c, Deal_Made__c, Eligible__c, Final_Demand_Amount__c,
					   Final_Demand_Received__c, Final_Lien_Amt_Paid__c, General_Ledger__c, Health_Plan__c, HICN__c,
					   Id, LienAmount__c, Lien_Holder_File_ID__c, Lien_Type__c, Name, Notes__c, Payable_Entity__c,
					   Policy__c, Re_sweep_Sent__c, Stages__c, State__c, Status_ERISA__c, Status__c, Submitted__c,
					   Subro_Rep__c, Claimant__r.Name, Account__r.Lienholder_Contact__r.Name, Account__r.Name, 
					   Account__r.Lienholder_Contact__r.Email, Account__r.Lienholder_Contact__r.Phone, Claimant__r.DOL__c, 
					   Account__r.Lienholder_Contact__r.Fax, Account__r.BillingStreet, Account__r.BillingCity, 
					   Account__r.BillingState, Account__r.BillingPostalCode, 
					   (SELECT Date_End__c, Date_Start__c, Id, LienType__c
					    FROM LienEligibilityDates__r
					    order by Date_Start__c desc)
				FROM Lien__c 
				WHERE Id = :lId
				limit 1];
	}
@auraEnabled
	global static list<Lien__c> getLiens(string cId, string aId) {
		return [SELECT Account__c, Action__c, Claimant__c, Cleared__c, Date_No_Interest__c, Date_Payment__c, Substage__c, 
					   Date_Re_sweep_Sent__c, Date_Submitted__c, Deal_Made__c, Eligible__c, Final_Demand_Amount__c,
					   Final_Demand_Received__c, Final_Lien_Amt_Paid__c, General_Ledger__c, Health_Plan__c, HICN__c,
					   Id, LienAmount__c, Lien_Holder_File_ID__c, Lien_Type__c, Name, Notes__c, Payable_Entity__c,
					   Policy__c, Re_sweep_Sent__c, Stages__c, State__c, Status_ERISA__c, Status__c, Submitted__c,
					   Subro_Rep__c, Claimant__r.Name, Account__r.Lienholder_Contact__r.Name, Account__r.Name, 
					   Account__r.Lienholder_Contact__r.Email, Account__r.Lienholder_Contact__r.Phone, Claimant__r.DOL__c, 
					   Account__r.Lienholder_Contact__r.Fax, Account__r.BillingStreet, Account__r.BillingCity, 
					   Account__r.BillingState, Account__r.BillingPostalCode, 
					   (SELECT Date_End__c, Date_Start__c, Id, LienType__c
					    FROM LienEligibilityDates__r
					    order by Date_Start__c desc)
				FROM Lien__c 
				WHERE Claimant__c = :cId
				AND Action__c = :aId];
	}
@auraEnabled 
	public static list<Lien_Negotiated_Amounts__c> getLienAmtList(string lId) {
		return [SELECT Id, Lien__c, Lien_Amount__c, Lien_Amount_Date__c, Phase__c
				FROM Lien_Negotiated_Amounts__c
				WHERE Lien__c = :lId
				order by Lien_Amount_Date__c desc];
	}	
	
@auraEnabled
	public static list<Injury__c> getInjuries(string lId) {
		Lien__c l = [SELECT Action__c, Claimant__c FROM Lien__c WHERE Id = :lId];
		return getCInjuries(l.Claimant__c, l.Action__c);
	}	
@auraEnabled
	public static list<Injury__c> getCInjuries(string cId, string aId) {
		return [SELECT Action__c, Compensable__c, Claimant__c, Description_Long__c, Explant_Date__c, ICD_Code__c, Id, Implant_Date__c, Injury_Category__c,
					   Injury_Description__c, Last_Treatment_Date__c, Lien__c, Name, Non_Surgical_Treatment_Enhancer__c, Non_Surgical_Treatment__c,
					   Surgical_Facility__c, Surgical_Treatment_Enhancer__c, Total_Surgery_Per_Defendant__c, Treatment_Enhancer__c, Base_Category__c,
					   Settlement_Category__c
				FROM Injury__c
				WHERE Claimant__c = :cId
				AND Action__c = :aId];	
	}	
@auraEnabled
	public static list<Award__c> getAwards(string lId) {
		Lien__c l = [SELECT Action__c, Claimant__c FROM Lien__c WHERE Id = :lId];
		return getCAwards(l.Claimant__c, l.Action__c);
	}
@auraEnabled
	public static list<Award__c> getCAwards(string cId, string aId) {
		return [SELECT Id, Action__c, Claimant__c, Date_of_Award__c, Amount__c, Claimant__r.Name
				FROM Award__c
				WHERE Claimant__c = :cId
				AND Action__c = :aId];
	}
@auraEnabled
	public static list<Claimant__c> getClaimants (string aId) {
		return [SELECT Id, Name, Address__c, City__c, Email__c, Law_Firm__c, Phone__c, SSN__c, State__c, Zip__c,
					   DOB__c, DOD__c, Gender__c, ProvidioID__c, Status__c, Law_Firm__r.Name, Law_Firm__r.Phone, 
					   Law_Firm__r.Website
				FROM Claimant__c
				WHERE Id IN (SELECT Claimant__c FROM ActionClaimant__c WHERE Action__c = :aId)];
	}
}